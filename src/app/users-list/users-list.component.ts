import { Component, OnInit } from '@angular/core';
import { User } from '../shared/user';
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  usersList: User[] | null = null;
  title: string = 'Users list';

  constructor(private usersService: UsersService) {}

  ngOnInit(): void {
    this.usersService.getUsers().subscribe((res) => {
      this.usersList = res;
    });
  }
}
